/**
    @file ds18b20.h
    header file for 1-wire sensor ds18b20
    @author theadib@gmail.com, 2019
    
    SPDX short identifier: MIT
*/

#ifndef INC_DS18B20_H
#define INC_DS18B20_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

extern int w1_device_search(uint64_t *devices, size_t count);

extern int ds18b20_init(void *);
extern int ds18b20_reset(void);
extern int ds18b20_startconversion(void);
extern int ds18b20_read(int *value);
extern int ds18b20_startconversion_address(uint64_t address);
extern int ds18b20_read_address(int *value, uint64_t address);
extern float ds18b20_toworld(int value);

#ifdef __cplusplus
}
#endif

#endif

