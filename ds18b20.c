/**
    @file ds18b20.c
    
    implementation for 1 wire sensor ds18b20
    @author theadib@gmail.com, 2019
    
    SPDX short identifier: MIT
    
    this driver uses the ds2484 to access 1-wire interface
    example has been developed using olimex stm32e407 board 
    
    the temperature is represented as int8 deg celsius temperature + 4bit fractional part
    the fractional part can be reduced to increase conversion speed
    9bit:94msec
    10bit: 188msec
    11bit: 375msec
    12bit: 750msec <- default

    DS2484 I2C to 1Wire interface:
    1 - SLPZ, connect to +VCC to disable sleep
    2 - SDA
    3 - SCL
    4 - GND
    5 - IO, connect to DS18B20 to IO pin, no pullup needed
    6 - VCC
    
    example scratchpad reads
    B2.1.4B.46.7F.FF.E.10.8C.
    C9.1.4B.46.7F.FF.7.10.64.

    temp_lsb, temp_msb bit15...bit4:temperature in celsius, bit3..bit0:fractional part celsius/16
    
    measurements:
    startconversion skip_ROM/match_ROM: 4msec/ 11msec
    conversion:1sec
    read skip_ROM/match_ROM: 20msec/ 28msec
    search ROM code: 86msec
*/

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include "ds18b20.h"

#include "stm32f4xx_hal.h"

static I2C_HandleTypeDef *hi2c;


#define DS2484_I2C_ADDR (0x18 << 1)

/// valid register pointer used in set read pointer command
typedef enum {
    DS2484_REG_DCR = 0xc3,
    DS2484_REG_SR = 0xf0,
    DS2484_REG_READ = 0xe1,
    DS2484_REG_PCR = 0xb4,
} ds2484_register_t;

// bit mask for status register
typedef enum {
    DS2484_REG_SR_1WB = 0x01,   /// 1 if bus is busy
    DS2484_REG_SR_PPD = 0x02,   /// 1 if presence detected
    DS2484_REG_SR_SD = 0x04,    /// 1 if short detected
    DS2484_REG_SR_LL = 0x08,    /// locic level of bus, sampled on acknowledge bit of read command
    DS2484_REG_SR_RST = 0x10,    /// 1 after reset, cleared to 0 after config write command
    DS2484_REG_SR_SBR = 0x20,    /// single_bit_result
    DS2484_REG_SR_TSB = 0x40,    /// triplet_second_bit
    DS2484_REG_SR_DIR = 0x80,    /// branch_direction_taken
} ds2484_register_status_t;

/// valid command codes
typedef enum {
    DS2484_CMD_DEVICERESET = 0xf0,
    DS2484_CMD_SETREADPOINTER = 0xe1,   // followed by register code
    DS2484_CMD_WRITEDEVICECONFIGURATION = 0xd2, // followed by new config value
    DS2484_CMD_1WIRERESET = 0xb4, // performs 1wire reset pulse
    DS2484_CMD_1WIREWRITEBYTE = 0xa5, // has parameter data byte
    DS2484_CMD_1WIREREADBYTE = 0x96, // shifts data into read data register
    DS2484_CMD_1WIRETRIPLET = 0x78, // read 2(two) bits and write 1(one) bit
    
} ds2484_cmd_t;

/// valid 1wire commands
typedef enum {
    OWIRE_CMD_MATCHROMCODE = 0x55,  // follows the ROM address
    OWIRE_CMD_SKIPROMCODE = 0xcc,
    OWIRE_CMD_READSCRATCHPAD = 0xbe,    // then read8byte data + 1byte crc
    OWIRE_CMD_CONVERTTEMPERATURE = 0x44,    // then wait 1sec
    OWIRE_CMD_SEARCH = 0xf0,    // start search using triplets
} owire_cmd_t;


static uint8_t w1_crc8_table[] = {
	0, 94, 188, 226, 97, 63, 221, 131, 194, 156, 126, 32, 163, 253, 31, 65,
	157, 195, 33, 127, 252, 162, 64, 30, 95, 1, 227, 189, 62, 96, 130, 220,
	35, 125, 159, 193, 66, 28, 254, 160, 225, 191, 93, 3, 128, 222, 60, 98,
	190, 224, 2, 92, 223, 129, 99, 61, 124, 34, 192, 158, 29, 67, 161, 255,
	70, 24, 250, 164, 39, 121, 155, 197, 132, 218, 56, 102, 229, 187, 89, 7,
	219, 133, 103, 57, 186, 228, 6, 88, 25, 71, 165, 251, 120, 38, 196, 154,
	101, 59, 217, 135, 4, 90, 184, 230, 167, 249, 27, 69, 198, 152, 122, 36,
	248, 166, 68, 26, 153, 199, 37, 123, 58, 100, 134, 216, 91, 5, 231, 185,
	140, 210, 48, 110, 237, 179, 81, 15, 78, 16, 242, 172, 47, 113, 147, 205,
	17, 79, 173, 243, 112, 46, 204, 146, 211, 141, 111, 49, 178, 236, 14, 80,
	175, 241, 19, 77, 206, 144, 114, 44, 109, 51, 209, 143, 12, 82, 176, 238,
	50, 108, 142, 208, 83, 13, 239, 177, 240, 174, 76, 18, 145, 207, 45, 115,
	202, 148, 118, 40, 171, 245, 23, 73, 8, 86, 180, 234, 105, 55, 213, 139,
	87, 9, 235, 181, 54, 104, 138, 212, 149, 203, 41, 119, 244, 170, 72, 22,
	233, 183, 85, 11, 136, 214, 52, 106, 43, 117, 151, 201, 74, 20, 246, 168,
	116, 42, 200, 150, 21, 75, 169, 247, 182, 232, 10, 84, 215, 137, 107, 53
};

static int ds2484_status_register = -1; // uninitialized is <0, will be updated on _wait_idle()

static uint8_t w1_calc_crc8(uint8_t * data, int len)
{
	uint8_t crc = 0;

	while(len > 0) {
		crc = w1_crc8_table[crc ^ *data++];
        len--;
    }

	return crc;
}

/**
    @return 0 on success, -1 on error
    @param addr one of ds2484_register_t
*/
static int ds2484_select_register(uint8_t addr)
{
    uint8_t data[2] = { DS2484_CMD_SETREADPOINTER, addr };
    HAL_StatusTypeDef state = HAL_I2C_Master_Transmit(hi2c, DS2484_I2C_ADDR, data, 2, 100);
    if(state == HAL_OK) {
        return 0;
    }
    return -1;
}

// wait busy some time, return false on error i.e. still busy
#define DS2482_WAIT_IDLE_TIMEOUT	100
/**
 * Waits until the 1-wire interface is idle (not busy)
 *
 * @return true if idle
 */
static bool ds2484_wait_1wire_idle(void)
{
	int retries = 0;

	if (!ds2484_select_register(DS2484_REG_SR)) {
        uint8_t data[1];
		HAL_StatusTypeDef state;
        do {
            state = HAL_I2C_Master_Receive(hi2c, DS2484_I2C_ADDR, data, 1, 100);
		} while ((state == HAL_OK) && (data[0] & DS2484_REG_SR_1WB) &&
			 (++retries < DS2482_WAIT_IDLE_TIMEOUT));
        if(state == HAL_OK) {
            ds2484_status_register = data[0];
        }
	}

	if (retries >= DS2482_WAIT_IDLE_TIMEOUT) {
        return false;
    }

    return true;
}

static bool ds2484_haspresencedetected(void) {
    // indiceate error when there is a short on bus or presence bit not set
    if((ds2484_status_register & (DS2484_REG_SR_PPD | DS2484_REG_SR_SD)) != DS2484_REG_SR_PPD) {
        return false;
    }

    return true;
}

/**
 * Sends a command without a parameter
 * @param cmd	DS2482_CMD_RESET,
 *		DS2482_CMD_1WIRE_RESET,
 *		DS2482_CMD_1WIRE_READ_BYTE
 * @return -1 on failure, 0 on success
 */
static int ds2484_send_cmd(uint8_t cmd)
{
	if (HAL_I2C_Master_Transmit(hi2c, DS2484_I2C_ADDR, &cmd, 1, 100) != HAL_OK) {
		return -1;
    }

	return 0;
}

/**
 * Sends a command with a parameter
 * @param cmd	DS2482_CMD_WRITE_CONFIG,
 *		DS2482_CMD_1WIRE_SINGLE_BIT,
 *		DS2482_CMD_1WIRE_WRITE_BYTE,
 *		DS2482_CMD_1WIRE_TRIPLET
 * @param byte	The data to send
 * @return -1 on failure, 0 on success
 */
static int ds2484_send_cmd_data(uint8_t cmd, uint8_t byte)
{
    uint8_t data[2] = {cmd, byte };
	if (HAL_I2C_Master_Transmit(hi2c, DS2484_I2C_ADDR, data, 2, 100) != HAL_OK) {
		return -1;
    }

	return 0;
}

/**
 * Performs the read byte function.
 *
 * @param data	pointer to store received data
 * @return -1 on error, 0: success
 */
static int ds2482_w1_read_byte(uint8_t *data)
{
	/* Select the channel */
	ds2484_wait_1wire_idle();

	/* Send the read byte command */
	ds2484_send_cmd(DS2484_CMD_1WIREREADBYTE);

	/* Wait until 1WB == 0 */
	ds2484_wait_1wire_idle();

	/* Select the data register */
	ds2484_select_register(DS2484_REG_READ);

	/* Read the data byte */
	HAL_I2C_Master_Receive(hi2c, DS2484_I2C_ADDR, data, 1, 100);

	return 0;
}

/**
    triplet implementation used for tree search
    reads 2(two) id_bits and write 1(one)bit
    @param direction content of search bit, 0 for 0 otherwise 1
    @return bitmask of return b0:id_bit, b1:compl_id_bit, b2:search_direction
*/
static int ds2482_w1_triplet(int direction)
{
    int triplet = 0;

	ds2484_wait_1wire_idle();
    ds2484_send_cmd_data(DS2484_CMD_1WIRETRIPLET, (direction == 0)?0:0x80);
	ds2484_wait_1wire_idle();

    triplet = ds2484_status_register >> 5;

    return triplet;    
}

/*
    search for connected W1 devices
    @param devices pointer to table to write addresses
    @param count number of items in table
    @return number of W1 devices found, or <0 on ERROR
**/
int w1_device_search(uint64_t *devices, size_t count)
{
    size_t device_count = 0;
    uint64_t address_mask = 0;
    int last_difference = -1;

    do {
        uint64_t difference_mask = 0;
        uint64_t received_address = 0;
        last_difference = -1;

        ds2484_wait_1wire_idle();
        ds2484_send_cmd(DS2484_CMD_1WIRERESET);
        ds2484_wait_1wire_idle();
        if(ds2484_haspresencedetected() == false) {
            break;
        }
        ds2484_send_cmd_data(DS2484_CMD_1WIREWRITEBYTE, OWIRE_CMD_SEARCH);
        for(int bit_position = 0; bit_position < 64; bit_position++) {
            int triplet = ds2482_w1_triplet((address_mask&(1ULL<<bit_position))!=0);

            if((triplet & 0x03) == 0x03) {
                // no device responded to that address
                break;
            } else if((triplet & 0x07) == 0x00) {
                // conflict and 0 choosen
                difference_mask |= 1ULL<<bit_position;
                last_difference = bit_position;
            }
            // update bit from data
            received_address |= ((uint64_t)((triplet&4)!=0)) << bit_position;
            
        }
        // update address mask
        if(last_difference >= 0) {
            address_mask = (1ULL << last_difference) | (received_address & ((1ULL << last_difference) - 1));
        }

        if((received_address>>56) == w1_calc_crc8((uint8_t *)&received_address, 7)) {
            devices[device_count] = received_address;
            device_count++;
        }
    } while((last_difference >= 0) && (device_count < count));

    return device_count;
}

/**
    driver initialisation
    @param hal, pointer to external hi2c reference
    @return <0 on error, 0 success
*/
int ds18b20_init(void *hal)
{
    hi2c = (I2C_HandleTypeDef *)hal;

    uint8_t data[1] = {0x00};
    HAL_I2C_Mem_Read(hi2c, DS2484_I2C_ADDR, 0xf0, 1, data, 1, 100);
    if(data[0] == 0x18) {
        // write device config
        HAL_I2C_Mem_Read(hi2c, DS2484_I2C_ADDR, 0xd2e1, 2, data, 1, 100);
        if(data[0] == 0x01) {
            return 0;
        }
    }
    
    return -1;
}

/**
    start the conversion for temperature
    @return 0 success
    @todo implement error handling
*/
int ds18b20_startconversion(void)
{
    ds2484_wait_1wire_idle();
    ds2484_send_cmd(DS2484_CMD_1WIRERESET);
    ds2484_wait_1wire_idle();
    ds2484_send_cmd_data(DS2484_CMD_1WIREWRITEBYTE, OWIRE_CMD_SKIPROMCODE);
    ds2484_wait_1wire_idle();
    // check bus feedback
    ds2484_send_cmd_data(DS2484_CMD_1WIREWRITEBYTE, OWIRE_CMD_CONVERTTEMPERATURE);
    ds2484_wait_1wire_idle();
    // check bus feedback
    
    return 0;
}

/**
    start the conversion for temperature
    @return 0 success
    @todo implement error handling
*/
int ds18b20_startconversion_address(uint64_t address)
{
    ds2484_wait_1wire_idle();
    ds2484_send_cmd(DS2484_CMD_1WIRERESET);
    ds2484_wait_1wire_idle();
    ds2484_send_cmd_data(DS2484_CMD_1WIREWRITEBYTE, OWIRE_CMD_MATCHROMCODE);
    ds2484_wait_1wire_idle();
    for(unsigned i = 0; i < 8; i++) {
        ds2484_send_cmd_data(DS2484_CMD_1WIREWRITEBYTE, address>>(i*8));
        ds2484_wait_1wire_idle();
    }
    // check bus feedback
    ds2484_send_cmd_data(DS2484_CMD_1WIREWRITEBYTE, OWIRE_CMD_CONVERTTEMPERATURE);
    ds2484_wait_1wire_idle();
    // check bus feedback
    
    return 0;
}

/**
    read converted values
    the calling code should wait after iniitating the conversion for good result
    @param pointer to value for storing result
    @return <0 on error, 0 success
*/
int ds18b20_read(int *value)
{
    ds2484_wait_1wire_idle();
    ds2484_send_cmd(DS2484_CMD_1WIRERESET);
    ds2484_wait_1wire_idle();
    ds2484_send_cmd_data(DS2484_CMD_1WIREWRITEBYTE, OWIRE_CMD_SKIPROMCODE);
    ds2484_wait_1wire_idle();
    // check bus feedback
    ds2484_send_cmd_data(DS2484_CMD_1WIREWRITEBYTE, OWIRE_CMD_READSCRATCHPAD);
    ds2484_wait_1wire_idle();
    // check bus feedback
    uint8_t scratchpad[9];
    for(int i = 0; i < 9; ++i) {
        ds2482_w1_read_byte(&scratchpad[i]);
    }
    
    // check crc
    if(scratchpad[8] != w1_calc_crc8(scratchpad, 8)) {
        return -1;
    }
    
    *value = ((int)scratchpad[1] << 8)|scratchpad[0];
    
    return 0;
}

/**
    read converted values
    the calling code should wait after iniitating the conversion for good result
    @param pointer to value for storing result
    @return <0 on error, 0 success
*/
int ds18b20_read_address(int *value, uint64_t address)
{
    ds2484_wait_1wire_idle();
    ds2484_send_cmd(DS2484_CMD_1WIRERESET);
    ds2484_wait_1wire_idle();
    ds2484_send_cmd_data(DS2484_CMD_1WIREWRITEBYTE, OWIRE_CMD_MATCHROMCODE);
    ds2484_wait_1wire_idle();
    for(unsigned i = 0; i < 8; i++) {
        ds2484_send_cmd_data(DS2484_CMD_1WIREWRITEBYTE, address>>(i*8));
        ds2484_wait_1wire_idle();
    }
    // check bus feedback
    ds2484_send_cmd_data(DS2484_CMD_1WIREWRITEBYTE, OWIRE_CMD_READSCRATCHPAD);
    ds2484_wait_1wire_idle();
    // check bus feedback
    uint8_t scratchpad[9];
    for(int i = 0; i < 9; ++i) {
        ds2482_w1_read_byte(&scratchpad[i]);
    }
    
    // check crc
    if(scratchpad[8] != w1_calc_crc8(scratchpad, 8)) {
        return -1;
    }
    
    *value = ((int)scratchpad[1] << 8)|scratchpad[0];
    
    return 0;
}

/// convert register value to real world celsius
float ds18b20_toworld(int value)
{
    return value / 16.0f;
}



