
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include "main.h"
#include "SEGGER_RTT.h"

#include "ds18b20.h"

extern I2C_HandleTypeDef hi2c1;


void app_init(void)
{
    int result = ds18b20_init(&hi2c1);
    SEGGER_RTT_printf(0, "\n\n\ninit ds18b20:%d. ", result);
    
}

void app_idle(void)
{
    static unsigned int loop = 0;
    SEGGER_RTT_printf(0, "\n\nloop:%u. ", loop);
    loop++;
    HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, 0);
    HAL_Delay(100);
    HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, 1);
    HAL_Delay(1500);

    uint64_t devices[10];
    int device_count = w1_device_search(devices, 10);
    SEGGER_RTT_printf(0, "\nfound %d devices.", device_count);
    if(device_count > 1) {
        for(int i = 0; i < device_count; i++) {
            char buffer[30];
            int bufcount = sprintf(buffer, "\n%d - %08lX:%08lX. ", i+1, (long)(devices[i]/0x100000000ULL), (long)devices[i]);
            SEGGER_RTT_Write(0, buffer, bufcount);

            int result = ds18b20_startconversion_address(devices[i]);
            SEGGER_RTT_printf(0, "start:%d. ", result);
            
            // wait conversion
            HAL_Delay(1000);

            int value;
            result = ds18b20_read_address(&value, devices[i]);
            SEGGER_RTT_printf(0, "read:%d - %04x. ", result, value);
            float temperature = ds18b20_toworld(value);
            sprintf(buffer, "convert:%d - %d %f. ", value, value/16, temperature);
            SEGGER_RTT_WriteString(0, buffer);
        }
    } else if(device_count == 1) {
        char buffer[30];
        int bufcount = sprintf(buffer, "\n%d - %08lX:%08lX. ", 1, (long)(devices[0]/0x100000000ULL), (long)devices[0]);
        SEGGER_RTT_Write(0, buffer, bufcount);

        int result = ds18b20_startconversion();
        SEGGER_RTT_printf(0, "start:%d. ", result);
        
        // wait conversion
        HAL_Delay(1000);

        int value;
        result = ds18b20_read(&value);
        SEGGER_RTT_printf(0, "read:%d - %04x. ", result, value);
        float temperature = ds18b20_toworld(value);
        sprintf(buffer, "convert:%d - %d %f. ", value, value/16, temperature);
        SEGGER_RTT_WriteString(0, buffer);
    }
}
